#include <string>
#include <iostream>
#include <math.h>


void cylinder(float h, float r, int s, float* arr){
  //returns an array of 6 * 3 * 4 * s floats representing the cylinder
  float ang = 6.2831853/s;

  int iter = 0;
  for(int i = 0; i < s; i++){
    arr[iter++] = 0;
    arr[iter++] = h/2;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;

    arr[iter++] = cos(ang*i)*r;
    arr[iter++] = h/2;
    arr[iter++] = sin(ang*i)*r;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;

    arr[iter++] = cos(ang*i+ang)*r;
    arr[iter++] = h/2;
    arr[iter++] = sin(ang*i+ang)*r;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;

    arr[iter++] = cos(ang*i)*r;
    arr[iter++] = h/2;
    arr[iter++] = sin(ang*i)*r;
    arr[iter++] = 0;
    arr[iter++] = 1;
    arr[iter++] = 0;

    arr[iter++] = cos(ang*i+ang)*r;
    arr[iter++] = h/2;
    arr[iter++] = sin(ang*i+ang)*r;
    arr[iter++] = 0;
    arr[iter++] = 1;
    arr[iter++] = 0;

    arr[iter++] = cos(ang*i+ang)*r;
    arr[iter++] = -h/2;
    arr[iter++] = sin(ang*i+ang)*r;
    arr[iter++] = 0;
    arr[iter++] = 1;
    arr[iter++] = 0;

    arr[iter++] = cos(ang*i)*r;
    arr[iter++] = -h/2;
    arr[iter++] = sin(ang*i)*r;
    arr[iter++] = 1;
    arr[iter++] = 0;
    arr[iter++] = 0;

    arr[iter++] = cos(ang*i+ang)*r;
    arr[iter++] = -h/2;
    arr[iter++] = sin(ang*i+ang)*r;
    arr[iter++] = 1;
    arr[iter++] = 0;
    arr[iter++] = 0;

    arr[iter++] = cos(ang*i)*r;
    arr[iter++] = h/2;
    arr[iter++] = sin(ang*i)*r;
    arr[iter++] = 1;
    arr[iter++] = 0;
    arr[iter++] = 0;

    arr[iter++] = 0;
    arr[iter++] = -h/2;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;

    arr[iter++] = cos(ang*i)*r;
    arr[iter++] = -h/2;
    arr[iter++] = sin(ang*i)*r;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;

    arr[iter++] = cos(ang*i+ang)*r;
    arr[iter++] = -h/2;
    arr[iter++] = sin(ang*i+ang)*r;
    arr[iter++] = 0;
    arr[iter++] = 0;
    arr[iter++] = 1;
  }

}

void box(float h, float w, float l, float* arr){
  //returns an array of 36 floats representing the cylinder

  float p[36*6];

  int iter = 0;

  for(int i = 0; i < 2; i++){
    float ni = (i-.5)*2;
    for(int j = 0; j< 2; j++){
      float nj = (j-.5)*2;
      for(int tri = 0; tri<3; tri++){
        for(int point = 0; point<3;point++){
          p[iter++] = ni*nj;
          p[iter++] = ni*(nj*-1);
          p[iter++] = ni;
        }
      }
      //std::cout << ni*nj << ni*(nj*-1) << ni <<std::endl;
    }
  }

  std::cout << *p;

}

//int main(void){
  /*
  float p[36];

  box(1,1,1,p);*/
  /*float p[6 * 3 * 4 * 10];
  cylinder(.5,.5,10,p);

  return 0;

}*/
