#ifndef MATRIX4_H
#define MATRIX4_H

#include <string>
#include <iostream>
#include <math.h>

#define PI 3.14159265

class Matrix4{
public:
    float m[16];

    Matrix4(){
        std::cout << "Matrix4 Constructor" <<std::endl;
        for(int i = 0; i < 16; ++i){
          m[i] = 0;
        }

    }

    ~Matrix4(){
        std::cout << "Matrix4 Destructor" <<std::endl;
    }

    void identity(){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }
    }

    void scale(float scale){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = (i == j);
        }
      }
      m[0] = scale;
      m[5] = scale;
      m[10] = scale;
    }

    void scale(float scale_x, float scale_y, float scale_z){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = (i == j);
        }
      }
      m[0] = scale_x;
      m[5] = scale_y;
      m[10] = scale_z;
    }

    void rotate_x(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }

      m[5] = cos(degree*PI/180);
      m[6] = -sin(degree*PI/180);
      m[9] = sin(degree*PI/180);
      m[10] = cos(degree*PI/180);
    }
    void rotate_y(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }

      m[0] = cos(degree*PI/180);
      m[8] = -sin(degree*PI/180);
      m[2] = sin(degree*PI/180);
      m[10] = cos(degree*PI/180);
    }
    void rotate_z(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }

      m[0] = cos(degree*PI/180);
      m[1] = -sin(degree*PI/180);
      m[4] = sin(degree*PI/180);
      m[5] = cos(degree*PI/180);
    }


    void rotate(float x, float y, float z){
      x = x*PI/180;
      y = y*PI/180;
      z = z*PI/180;
      m[0] = cos(z)*cos(y);
      m[1] = cos(z)*sin(y)*sin(x)-sin(z)*cos(x);
      m[2] = cos(z)*sin(y)*cos(x)+sin(z)*sin(x);
      m[3] = 0;
      m[4] = sin(z)*cos(y);
      m[5] = sin(z)*sin(y)*sin(x)+cos(z)*cos(x);
      m[6] = sin(z)*sin(y)*cos(x)-cos(z)*sin(x);
      m[7] = 0;
      m[8] = -sin(y);
      m[9] = cos(y)*sin(x);
      m[10] = cos(y)*cos(x);
      m[11] = 0;
      m[12] = 0;
      m[13] = 0;
      m[14] = 0;
      m[15] = 1;

      m[0] = cos(x)*cos(y);
      m[1] = (cos(x)*sin(y)*sin(z))-(sin(x)*cos(z));
      m[2] = cos(x)*sin(y)*cos(z)+(sin(x)*sin(z));
      m[3] = 0;
      m[4] = sin(x)*cos(y);
      m[5] = (sin(x)*sin(y)*sin(z))+(cos(x)*cos(z));
      m[6] = (sin(x)*sin(y)*cos(z))-(cos(x)*sin(z));
      m[7] = 0;
      m[8] = -sin(y);
      m[9] = cos(y)*sin(z);
      m[10] = cos(y)*cos(z);
      m[11] = 0;
      m[12] = 0;
      m[13] = 0;
      m[14] = 0;
      m[15] = 1;
    }

    void translate(float x, float y, float z){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }
      m[3] = x;
      m[7] = y;
      m[11] = z;
    }

    void perspective(){
      float n = -5;
      float f = 6;
      float l = -1;
      float r = 1;
      float t = 1;
      float b = -1;
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          m[i*4 + j] = i == j;
        }
      }
      m[10] = -f/(f-n);
      //m[10]=1;
      m[11] = -1;
      m[14] = -(f*n)/(f-n);
      m[15] = 0;

      /*
      m[0] = 2*abs(n)/(r-l);
      m[2] = (r+l)/(r-l);
      m[5] = 2*abs(n)/(t-b);
      m[6] = (t+b)/(t-b);
      m[10] = (abs(n)+abs(f))/(abs(n)-abs(f));
      m[11] = (2*abs(n)*abs(f))/(abs(n)-abs(f));
      m[14] = -1;
      m[15] = 0;
      */
    }

    void print(){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          std::cout << m[i*4 + j] << " \n"[j == 3];
        }
      }
    }

};

Matrix4 operator+(Matrix4& m1, Matrix4& m2){
	Matrix4 m4 = Matrix4();
  for(int i = 0; i < 16; ++i){
    m4.m[i] = m1.m[i] + m2.m[i];
  }

  return m4;
}

Matrix4 operator*(Matrix4& m1, Matrix4& m2){
	Matrix4 m4 = Matrix4();
  for(int i = 0; i < 16; ++i){
    float sum_i = 0;
    for(int j = 0; j < 4; ++j){
      sum_i = sum_i + (m1.m[i%4 + j*4] * m2.m[int(i/4)*4 + j]);
    }
    m4.m[i] = sum_i;
  }

  return m4;
}

#endif
