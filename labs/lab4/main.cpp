#include "Matrix4.cpp"
#include "generate_primatives.cpp"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>

#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>




void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}


int processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){
        return 1;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
        return 2;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
        return 3;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
        return 4;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
        return 5;
    }
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        return 6;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        return 7;
    }
    if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS){
        return 8;
    }
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS){
        return 9;
    }
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS){
        return 10;
    }
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS){
        return 11;
    }
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS){
        return 12;
    }
    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS){
        return 13;
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS){
        return 14;
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
        return 15;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS){
        return 16;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS){
        return 17;
    }
    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS){
        return 18;
    }
    return 0;
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();
    /** YOU WILL ADD CODE STARTING HERE */
    //GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    //GLuint program = 0;
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {


  GLFWwindow* window = initWindow();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  if (!window) {
      std::cout << "There was an error setting up the window" << std::endl;
      return 1;
  }


  /*float triangle[] = {
         0.5f,  0.5f, 0.0, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 0.0, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 0.0, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 0.0, 1.0, 0.0,
  };*/

  float cube[] = {
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
       0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

       0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
  };


  float cy[6 * 3 * 4 * 20];
  cylinder(.75,.25,20,cy);

  //float* triangle[6 * 3 * 4 * 20];

  GLuint VBO[1], VAO[1];

  glGenVertexArrays(1, &VAO[0]);
  glGenBuffers(1, &VBO[0]);
  // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
  glBindVertexArray(VAO[0]);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
  glEnableVertexAttribArray(1);

  GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
  GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

  // create the shader program
  // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
  GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

  // cleanup the vertex and fragment shaders using glDeleteShader
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  glEnable(GL_DEPTH_TEST);

  Matrix4 m = Matrix4();
  m.identity();
  Matrix4 c = Matrix4();
  c.identity();
  Matrix4 p = Matrix4();
  p.identity();
  Matrix4 test = Matrix4();
  //User input values:
  int input = 0;

  int shape = 0;

  int cam_y = -20;

  float model_x = 0;
  float model_y = 0;
  float model_z = 0;

  float model_yaw = 0;
  float model_pitch=0;
  float model_roll = 0;

  float model_scale = 1;

  int projection = 0;

  while (!glfwWindowShouldClose(window)) {

    float time = glfwGetTime();

    // you don't need to worry about processInput, all it does is listen
    // for the escape character and terminate when escape is pressed.
    input = processInput(window);
    //std::cout << input << std::endl;
    if(input == 1){
      shape += 2;
    }
    if(input != 1 && shape >= 2){
      shape = (shape - 1) %2;
    }


    if(input == 2 && cam_y > -20){
      cam_y--;
    }
    if(input == 3 && cam_y < 20){
      cam_y++;
    }
    if(input == 4){
      model_x++;
    }
    if(input == 5){
      model_x--;
    }
    if(input == 6){
      model_y++;
    }
    if(input == 7){
      model_y--;
    }
    if(input == 8){
      model_z++;
    }
    if(input == 9){
      model_z--;
    }
    if(input == 10){
      model_scale += .1;
    }
    if(input == 11){
      model_scale -= .1;
    }
    if(input == 12){
      model_pitch++;
    }
    if(input == 13){
      model_pitch--;
    }
    if(input == 14){
      model_yaw++;
    }
    if(input == 15){
      model_yaw--;
    }
    if(input == 16){
      model_roll++;
    }
    if(input == 17){
      model_roll--;
    }
    if(input == 18){
      projection += 2;
    }
    if(input != 18 && projection >= 2){
      projection = (projection -1)%2;
    }

    //camera
    c.translate(0,(float)cam_y/40,0);
    test.rotate_x(atan((float)cam_y/20)*180/3.14);
    test.identity();
    c = test * c;

    if(input != 0){

      //model translation
      m.translate(model_x/100,model_y/100,model_z/100);
      test.rotate(model_pitch,model_yaw,model_roll);
      m = test * m;
      test.scale(model_scale);
      m = test * m;

    }

    //projection
    if(projection == 0){
      p.perspective();
      test.translate(0,0,1.1);
      p = test * p;
    }
    if(projection == 1){
      p.identity();
    }


    /** YOU WILL ADD RENDERING CODE STARTING HERE */
    /** PART4: Implemting the rendering loop */

    // clear the screen with your favorite color using glClearColor

    glClearColor(0.0f, 0.3f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderProgram);
    int transformLocation = glGetUniformLocation(shaderProgram, "cam");
    glUniformMatrix4fv(transformLocation, 1, false, c.m );

    // set the shader program using glUseProgram
    glUseProgram(shaderProgram);
    transformLocation = glGetUniformLocation(shaderProgram, "transform");
    glUniformMatrix4fv(transformLocation, 1, false, m.m );

    glUseProgram(shaderProgram);
    transformLocation = glGetUniformLocation(shaderProgram, "projection");
    glUniformMatrix4fv(transformLocation, 1, false, p.m );

    // bind the vertex array using glBindVertexArray
    glBindVertexArray(VAO[0]);
    // draw the triangles using glDrawArrays
    //glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
    //glDrawArrays(GL_TRIANGLES, 0, sizeof(cube));
    if(shape%2 == 0){
      glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
      glDrawArrays(GL_TRIANGLES, 0, sizeof(cube));
    }
    if(shape%2 == 1){
      glBufferData(GL_ARRAY_BUFFER, sizeof(cy), cy, GL_STATIC_DRAW);
      glDrawArrays(GL_TRIANGLES, 0, sizeof(cy));
    }

    /** END RENDERING CODE */

    // Swap front and back buffers
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glfwTerminate();
  return 0;

  //glfwGetTime()

}
