#include <iostream>
#include <map>
#include <vector>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#include "camera.h"
#include "hit.h"
#include "intersector.h"
#include "light.h"
#include "ray.h"
#include "renderer.h"
#include "shape.h"
#include "timer.h"


class BruteForceIntersector : public Intersector {
public:

    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);
        for (auto surface : world.shapes()) {
            double cur_t = surface->intersect(ray);

            if (cur_t < hit.t()) {
                hit.update(surface, cur_t);
            }
        }
        return hit;
    }
};


class MySlickIntersector : public Intersector {
public:
    World w1;
    std::vector<std::vector<Triangle>> bound_boxes;
    //map stores a set of indexes to boxes and the things inside of them. If the 0th index of the vector is 0, the indexes represent more bounding boxes, if 1, it is an index to a shape in our world
    std::map<int, std::vector<int>> heir;
    std::vector<int> heir_top_lvl;

    MySlickIntersector(const World& world, int definition = 1): w1(world){
      std::cout << world.shapes().size() << std::endl;

      // std::vector<int> v;
      // v.push_back(1);
      // heir.insert(std::pair<int, std::vector<int>>(1, v));
      // heir.insert(std::pair<int, int>(2, 2));
      // int t = heir.at(1)[0];
      // std::cout << t << std::endl;

      std::vector<int> v;
      std::vector<int> curr_heir_top_lvl;

      int iter = 0;
      double bounds[6];
      for(auto surface : world.shapes()){
        surface->get_min_max(bounds);
        bound_boxes.push_back(Obj::make_box(glm::vec3(bounds[0],bounds[1],bounds[2]), glm::vec3(bounds[3],bounds[4],bounds[5]), glm::vec3(0,0,0)));
        iter++;
        v.clear();
        v.push_back(1);
        v.push_back(iter);
        heir.insert(std::pair<int, std::vector<int>>(iter, v));

        curr_heir_top_lvl.push_back(iter);
      }

      int combine = 0;
      int nearest[4];
      std::vector<int> available;
      while(combine <= definition){

        for(auto i: curr_heir_top_lvl){
          available.push_back(i);
        }
        //curr_heir_top_lvl.clear();
        while(available.size() > 0){
          //get first element
          //find 3 closest
          //get bounding box around all
          //add bound_box to list
          //add to curr_heir_top_lvl
          //remove 4 from available
          nearest[0] =  available[0];
          available.erase(available.begin());
          nearest[1] = find_closest_neighbor(nearest[0]);

        }

        combine ++;
      }


      heir_top_lvl = curr_heir_top_lvl;
      std::cout << heir_top_lvl.size() << std::endl;

    }

    int find_closest_neighbor(int target){
      double min_max_target[6];
      double min_max_check[6];
      get_min_max(bound_boxes[target], min_max_target);


      return -1;
    }

    void get_min_max(std::vector<Triangle> tris, double t[]) const{
      // double temp[6];
      // tris[0].get_min_max(t);
      // for(auto tri : tris){
      //   tri.get_min_max(temp);
      //   for(int i = 0; i < 3; i++){
      //     if(temp[i]<t[i]){
      //       t[i] = temp[i];
      //     }
      //   }
      //   for(int i = 3; i < 6; i++){
      //     if(temp[i]>t[i]){
      //       t[i] = temp[i];
      //     }
      //   }
      // }
    }

    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);
        // TODO: accelerate finding intersections with a spatial data structure.
        std::vector<int> check;
        for (int i=0; i<heir_top_lvl.size(); i++){
          check.push_back(heir_top_lvl[i]);
        }
        //int i = 0;
        std::vector<Triangle> current_bounds;
        std::vector<int> current;
        while(check.size() > 0){

          current_bounds = bound_boxes[check[0]];
          current = heir.at(check[0]);
          if(predicate(current_bounds,ray)){
            if(current[0] == 1){ //objects in this division are renderable objects
              for(int i = 0; i < current.size() - 1; i++){
                double cur_t = world.shapes()[current[i+1]]->intersect(ray);
                if (cur_t < hit.t()) {
                    hit.update(world.shapes()[current[i+1]], cur_t);
                }
              }
            }else if(current[0] == 0){//objects in this division are more subsections
              for(int i = 0; i < current.size() - 1; i++){
                if (predicate(bound_boxes[current[i+1]], ray)) {
                    check.push_back(current[i+1]);
                }
              }
            }
          }

          check.erase(check.begin());
        }
        return hit;
    }

    bool predicate(std::vector<Triangle> box, const Ray& ray){
      for(auto b : box){
        if( predicate(&b,ray) ){
          return true;
        }
      }
      return false;
    }

    bool predicate(Shape* box, const Ray& ray){
      // std::cout << box.intersect(ray) << std::endl;
      // std::cout << ray.direction[0] << std::endl;
      // std::cout << box.id() << std::endl;
      // exit(1);
      if( box->intersect(ray) != std::numeric_limits<double>::infinity() ){
        return true;
      }
      //std::cout << "intersects" << std::endl;
      return false;
    }
};


double rand_val() {
    static bool init = true;
    if (init) {
        srand(time(NULL));
        init = false;
    }
    return ((double) rand() / (RAND_MAX));
}

glm::vec3 rand_color() {
    return glm::vec3(rand_val(),rand_val(),rand_val());
}


std::vector<Triangle> random_box() {
    float  x = (rand_val() * 8) - 4;
    float  y = (rand_val() * 8) - 4;
    float  z = rand_val() * 5;
    float scale = rand_val() * 2;
    return Obj::make_box(glm::vec3(x, y, z), scale, rand_color());
}


int main(int argc, char** argv) {

    // set the number of boxes
    int NUM_BOXES = 10;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // setup the camera
    float dist_to_origin = 5;
    Camera camera(
            glm::vec3(0, 0, -dist_to_origin),   // eye
            glm::vec3(0, 0, 0),                 // target
            glm::vec3(0, 1, 0),                 // up
            glm::vec2(-5, -5),                  // viewport min
            glm::vec2(5, 5),                    // viewport max
            dist_to_origin,                     // distance from eye to view plane
            glm::vec3(.3, .6, .8)               // background color
    );

    // setup lights
    // see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
    // for good attenuation value.
    // I found the values at 7 to be nice looking
    PointLight l1(glm::vec3(1, 1, 1), glm::vec3(3, -3, 0), 1.0, .7, 0.18);
    DirectionalLight l2(glm::vec3(.5, .5, .5), glm::vec3(-5, 4, -1));
    Lights lights = { &l1, &l2 };

    // setup world
    World world;

    // add the light
    world.append(Sphere(l1.position(), .25, glm::vec3(1,1,1)));

    // and the spheres
    world.append(Sphere(glm::vec3(1, 1, 1), 1, rand_color()));
    world.append(Sphere(glm::vec3(2, 2, 4), 2, rand_color()));
    world.append(Sphere(glm::vec3(3, 3, 6), 3, rand_color()));

    //world.append(Obj::make_box(glm::vec3(0, 0, 0), glm::vec3(-1, -2, -2), rand_color()));
    std::vector<Triangle> obj = Obj::make_box(glm::vec3(-4, 4, 0), glm::vec3(-1, 2, 5), rand_color());
    //world.append(obj);

    // and add some boxes and prep world for rendering
    for (int i = 0 ; i < NUM_BOXES ; ++i) {
        world.append(random_box());
    }
    world.lock();

    // create the intersector
    //BruteForceIntersector intersector;
    MySlickIntersector intersector = MySlickIntersector(world);

    // and setup the renderer
    Renderer renderer(&intersector);

    // render
    Timer timer;
    timer.start();
    renderer.render(image, camera, lights, world);
    timer.stop();

    image.save_image("ray-traced.bmp");
    std::cout << "Rendered in " <<  timer.total() << " milliseconds" << std::endl;
}
