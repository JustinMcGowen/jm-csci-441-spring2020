#ifndef MATRIX3_H
#define MATRIX3_H

#include <string>
#include <iostream>
#include <math.h>

#define PI 3.14159265

class Matrix3{
public:
    float m[9];

    Matrix3(){
        std::cout << "Matrix3 Constructor" <<std::endl;
        for(int i = 0; i < 9; ++i){
          m[i] = 0;
        }

    }

    ~Matrix3(){
        std::cout << "Matrix3 Destructor" <<std::endl;
    }

    void identity(){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          m[i*3 + j] = i == j;
        }
      }
    }

    void scale(float scale){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          m[i*3 + j] = (i == j);
        }
      }
      m[0] = scale;
      m[4] = scale;
    }

    void scale(float scale_x, float scale_y){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          m[i*3 + j] = (i == j);
        }
      }
      m[0] = scale_x;
      m[4] = scale_y;
    }

    void rotate(float degree){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          m[i*3 + j] = i == j;
        }
      }

      m[0] = cos(degree*PI/180);
      m[1] = -sin(degree*PI/180);
      m[3] = sin(degree*PI/180);
      m[4] = cos(degree*PI/180);
    }

    void translate(float x, float y){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          m[i*3 + j] = i == j;
        }
      }
      m[2] = x;
      m[5] = y;
    }

    void print(){
      for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
          std::cout << m[i*3 + j] << " \n"[j == 2];
        }
      }
    }

};

Matrix3 operator+(Matrix3& m1, Matrix3& m2){
	Matrix3 m3 = Matrix3();
  for(int i = 0; i < 9; ++i){
    m3.m[i] = m1.m[i] + m2.m[i];
  }

  return m3;
}

Matrix3 operator*(Matrix3& m1, Matrix3& m2){
	Matrix3 m3 = Matrix3();
  for(int i = 0; i < 9; ++i){
    float sum_i = 0;
    for(int j = 0; j < 3; ++j){
      sum_i = sum_i + (m1.m[i%3 + j*3] * m2.m[int(i/3)*3 + j]);
    }
    m3.m[i] = sum_i;
  }

  return m3;
}

#endif
