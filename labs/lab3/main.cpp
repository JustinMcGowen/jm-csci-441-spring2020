#include "Matrix3.cpp"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>

#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>



void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

int processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){
        return 1;
    }
    return 0;
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();
    /** YOU WILL ADD CODE STARTING HERE */
    //GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    //GLuint program = 0;
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {

  Matrix3 identity = Matrix3();
  identity.identity();
  Matrix3 test = Matrix3();
  test.m[0] = 60;
  test.m[1] =50;
  test.m[2] = 70;
  test.m[3] = 50;
  test.m[4] = 60;
  test.m[5] = 70;
  test.m[6] = 50;
  test.m[7] =  70;
  test.m[8] = 60;
  //identity.scale(.5);
  //identity.rotate(180);
  //identity.translate(2,1);
  identity.print();
  test.print();
  test = test * identity;
  test.print();


  GLFWwindow* window = initWindow();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  if (!window) {
      std::cout << "There was an error setting up the window" << std::endl;
      return 1;
  }


  float triangle[] = {
         0.5f,  0.5f, 1.0, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 1.0, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 1.0, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 1.0, 0.0, 1.0, 0.0,
  };

  GLuint VBO[1], VAO[1];

  glGenVertexArrays(1, &VAO[0]);
  glGenBuffers(1, &VBO[0]);
  // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
  glBindVertexArray(VAO[0]);

  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
  glEnableVertexAttribArray(1);

  GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
  GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

  // create the shader program
  // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
  GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

  // cleanup the vertex and fragment shaders using glDeleteShader
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  int prog = 3;
  Matrix3 m = Matrix3();

  int space_pressed = 0;
  int space_released = 0;

  while (!glfwWindowShouldClose(window)) {

    float time = glfwGetTime();

    prog = prog % 5;
    if(prog == 0){
      m.identity();
    }
    if(prog == 1){
        m.scale(sin(time));
    }
    if(prog == 2){
      m.rotate(time*10);
    }
    if(prog == 3){
      m.translate(.4*sin(time),.1*sin(time));
    }
    if(prog == 4){
      m.rotate(time*20);
      test.scale(sin(time));
      m = m*test;
      test.translate(.6,.4);
      m = m * test;
    }
      // you don't need to worry about processInput, all it does is listen
      // for the escape character and terminate when escape is pressed.
      space_pressed = space_released;
      space_released = processInput(window);

      if(space_pressed == 1 && space_released == 0){
        ++prog;
      }

      /** YOU WILL ADD RENDERING CODE STARTING HERE */
      /** PART4: Implemting the rendering loop */

      // clear the screen with your favorite color using glClearColor

      glClearColor(0.0f, 0.3f, 0.1f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);

      // set the shader program using glUseProgram
      glUseProgram(shaderProgram);
      int transformLocation = glGetUniformLocation(shaderProgram, "transform");
      glUniformMatrix3fv(transformLocation, 1, false, m.m );

      // bind the vertex array using glBindVertexArray
      glBindVertexArray(VAO[0]);
      // draw the triangles using glDrawArrays
      glDrawArrays(GL_TRIANGLES, 0, 6);


      /** END RENDERING CODE */

      // Swap front and back buffers
      glfwSwapBuffers(window);
      glfwPollEvents();
  }

  glfwTerminate();
  return 0;

  //glfwGetTime()
}
