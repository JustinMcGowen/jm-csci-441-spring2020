#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_

class Model {

public:
    GLuint vbo;
    GLuint vao;
    GLuint ebo;
    Shader shader;
    Matrix4 model;
    int size;
    int idxSize;

    template <typename Coords, typename Idx>
    Model(const Coords& coords, const Idx& indicies, const Shader& shader_in) : shader(shader_in) {
        size = coords.size()*sizeof(float);
        idxSize = indicies.size()*sizeof(int);

        // copy vertex data
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_DYNAMIC_DRAW);

        glGenBuffers(1, &ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, idxSize, indicies.data(), GL_DYNAMIC_DRAW);

        // describe vertex layout
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(0*sizeof(float)));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2);
    }

    template <typename Coords, typename Idx>
    void changeShading(const Coords& coords, const Idx& indicies){
      size = coords.size()*sizeof(float);
      idxSize = indicies.size()*sizeof(int);

      // change vertex data
      //glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_STATIC_DRAW);
      glDeleteBuffers(1, &vbo);
      glDeleteBuffers(1, &ebo);
      glDeleteBuffers(1, &vao);
      //glClearBufferfv(GL_COLOR, vbo, cl);
      glGenBuffers(1, &vbo);
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_DYNAMIC_DRAW);

      glGenBuffers(1, &ebo);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, idxSize, indicies.data(), GL_DYNAMIC_DRAW);

      // describe vertex layout
      glGenVertexArrays(1, &vao);
      glBindVertexArray(vao);

      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
              (void*)(0*sizeof(float)));
      glEnableVertexAttribArray(0);

      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
              (void*)(3*sizeof(float)));
      glEnableVertexAttribArray(1);

      glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
              (void*)(6*sizeof(float)));
      glEnableVertexAttribArray(2);
    }
};

#endif
