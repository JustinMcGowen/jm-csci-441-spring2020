#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

//export MESA_GL_VERSION_OVERRIDE=3.3

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */
 class Point{
 public:
     float x;
     float y;
     float r;
     float g;
     float b;

     Point(int xx=0, int yy=0, float rr=0, float gg=0, float bb=0): x(xx), y(yy), r(rr), g(gg), b(bb){
         std::cout << "Point Constructor" <<std::endl;

     }

     ~Point(){
         std::cout << "Point Destructor" <<std::endl;
     }
     void stop(std::string s){
         x = 1 - std::stof(s.substr(0,s.find(",")))*2/640;
         s = s.substr(s.find(",")+1,-1);
         y = 1 - std::stof(s.substr(0,s.find(":")))*2/480;
         s = s.substr(s.find(":")+1,-1);
         r = std::stof(s.substr(0,s.find(",")));
         s = s.substr(s.find(",")+1,-1);
         g = std::stof(s.substr(0,s.find(",")));
         s = s.substr(s.find(",")+1,-1);
         b = std::stof(s.substr(0,-1));
     }
     void print(){
       std::cout << x << ", "<< y << ": "<< r << ", "<< g << ", "<< b << ", "<<std::endl;
     }

 };

 int max(int n_args, int x, int y, int z){
     if(x>=y && x>=z){return x;}
     if(y>=x && y>=z){return y;}
     if(z>=x && z>=y){return z;}
     return 0;
 }
 float gamma(Point& p1, Point& p2, Point& p3, Point& target){
     int g1 = (p2.y - p3.y)*(target.x - p3.x) + (p3.x - p2.x)*(target.y - p3.y);
     int g2 = (p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y);
     return (float)g1/(float)g2;
 }

 float beta(Point& p1, Point& p2, Point& p3, Point& target){
     int b1 = (p3.y - p1.y)*(target.x - p3.x) + (p1.x - p3.x)*(target.y - p3.y);
     int b2 = (p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y);
     return (float)b1/(float)b2;
 }
 float alpha(Point& p1, Point& p2, Point& p3, Point& target){
     float b = beta(p1,p2,p3,target);
     float g = gamma(p1,p2,p3,target);
     return 1 - b - g;
 }



void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();
    /** YOU WILL ADD CODE STARTING HERE */
    //GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    //GLuint program = 0;
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {

    GLFWwindow* window = initWindow();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */

     //50,50:1,0,0
     //600,20:0,1,0
     //300,400:0,0,1

     std::string p;

     std::cout << "Enter 3 points (enter a point as x,y:r,g,b):";
     std::cin >> p;
     Point p1;
     p1.stop(p);
     std::cin >> p;
     Point p2;
     p2.stop(p);
     std::cin >> p;
     Point p3;
     p3.stop(p);
     p1.print();
     p2.print();
     p3.print();

     float triangle[] = {
          p1.x, p1.y, 0.0f, p1.r, p1.g, p1.b,
          p2.x, p2.y, 0.0f, p2.r, p2.g, p2.b,
          p3.x, p3.y, 0.0f, p3.r, p3.g, p3.b
     };
     /*
     float triangle[] = {
       -0.9f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, // left
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // right
        0.0f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f // top
     };*/

    /** PART2: map the data */

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    GLuint VBO[1], VAO[1];

    //TODO: test
    glGenVertexArrays(1, &VAO[0]);
    glGenBuffers(1, &VBO[0]);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO[0]);

    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);

    //glBindVertexArray(0);

    //END TODO

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData

    // setup the attribute pointer for the coordinates
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;

    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor

        glClearColor(0.0f, 0.3f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram
        glUseProgram(shaderProgram);
        // bind the vertex array using glBindVertexArray
        glBindVertexArray(VAO[0]);
        // draw the triangles using glDrawArrays
        glDrawArrays(GL_TRIANGLES, 0, 3);


        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
