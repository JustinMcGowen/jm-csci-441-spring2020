#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray {
    int id;
    glm::vec3 origin;
    glm::vec3 direction;

    Ray(const glm::vec3& origin=glm::vec3(0,0,0),
            const glm::vec3& direction=glm::vec3(0,0,0))
        : origin(origin),direction(direction) {
            static int ray_id = 0;
            id = ++ray_id;
        }
};

const int IMG_WIDTH = 640;
const int IMG_HEIGHT = 480;

int intersects(const Ray& ray, const std::vector<Sphere>& objects){
  int intersect_idx = -1;
  int dist = -1;

  for(int i = 0; i < objects.size(); i++){
    Sphere curr = objects[i];
    glm::vec3 oc = ray.origin - curr.center;
    float a = glm::dot(-ray.direction, -ray.direction);
    float b = 2.0 * dot(oc, -ray.direction);
    float c = dot(oc,oc) - curr.radius*curr.radius;
    float discriminant = b*b - 4*a*c;
    if(discriminant>=0){
      if(((-b - sqrt(discriminant)) / (2.0*a) ) < dist || dist == -1){
        dist = (-b - sqrt(discriminant)) / (2.0*a);
        intersect_idx = i;
      }
    }
  }

  return intersect_idx;
}


void render(bitmap_image& image, const std::vector<Sphere>& world, const char mode) {
    float ui;
    float vj;

    std::vector<Ray> view ={};
    if(mode == 'o'){
      //orthagraphic rays
      std::vector<Ray> ortho ={};
      for(int i = 0; i< IMG_WIDTH; i++){
        for(int j = 0; j< IMG_HEIGHT; j++){
          //ui = l + (r-l)*(i+.5) / nx
          //vj = b + (t-b)*(j+.5) / ny,
          ui = -10/2 + 10*(i+.5) / IMG_WIDTH;
          vj = 10/2 - 10*(j+.5) / IMG_HEIGHT;
          view.push_back(Ray(glm::vec3(ui,vj,0),glm::vec3(0,0,-1)));
        }
      }
    }else if(mode == 'p'){
      //perspective rays
      for(int i = 0; i< IMG_WIDTH; i++){
        for(int j = 0; j< IMG_HEIGHT; j++){
          float cam_offset = 1;
          ui = -10/2 + 10*(i+.5) / IMG_WIDTH;
          vj = 10/2 - 10*(j+.5) / IMG_HEIGHT;
          glm::vec3 view_plane = glm::vec3(ui,vj,0);
          glm::vec3 origin = glm::vec3(0,0,-cam_offset);
          view.push_back(Ray(origin,origin-view_plane));
        }
      }
    }else{}

    std::vector<int> intersections = {};
    int pixel_no = view.size();
    int intersect_idx = -1;
    glm::vec3 pix_color;
    //std::cout << pixel_no << std::endl;
    for(int i = 0; i< pixel_no; i++){
      intersect_idx = intersects(view[i], world);


      if(intersect_idx == -1){
        image.set_pixel(int(i/IMG_HEIGHT),i%IMG_HEIGHT, 75, 156, 211);
      }else{
        pix_color = world[intersect_idx].color;
        image.set_pixel(int(i/IMG_HEIGHT),i%IMG_HEIGHT, pix_color.x*255, pix_color.y*255, pix_color.z*255);
      }

    }


}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(IMG_WIDTH, IMG_HEIGHT);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
    };


    // render the world
    render(image, world, 'o');

    image.save_image("orthagraphic.bmp");

    render(image, world, 'p');

    image.save_image("perspective.bmp");
    std::cout << "Success" << std::endl;
}
