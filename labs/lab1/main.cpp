#include <iostream>
#include <string>

#include "bitmap_image.hpp"

class Point{
public:
    int x;
    int y;
    float r;
    float g;
    float b;

    Point(int xx=0, int yy=0, float rr=0, float gg=0, float bb=0): x(xx), y(yy), r(rr), g(gg), b(bb){
        std::cout << "Point Constructor" <<std::endl;

    }

    ~Point(){
        std::cout << "Point Destructor" <<std::endl;
    }
    void stop(std::string s){
        x = std::stoi(s.substr(0,s.find(",")));
        s = s.substr(s.find(",")+1,-1);
        y = std::stoi(s.substr(0,s.find(":")));
        s = s.substr(s.find(":")+1,-1);
        r = std::stof(s.substr(0,s.find(",")));
        s = s.substr(s.find(",")+1,-1);
        g = std::stof(s.substr(0,s.find(",")));
        s = s.substr(s.find(",")+1,-1);
        b = std::stof(s.substr(0,-1));
    }
    void print(){
      std::cout << x << ", "<< y << ": "<< r << ", "<< g << ", "<< b << ", "<<std::endl;
    }

};

int max(int n_args, int x, int y, int z){
    if(x>=y && x>=z){
      return x;
    }
    if(y>=x && y>=z){
      return y;
    }
    if(z>=x && z>=y){
      return z;
    }
    return 0;
}

int min(int n_args, int x, int y, int z){
    if(x<=y && x<=z){
      return x;
    }
    if(y<=x && y<=z){
      return y;
    }
    if(z<=x && z<=y){
      return z;
    }
    return 0;
}

float gamma(Point& p1, Point& p2, Point& p3, Point& target){
    int g1 = (p2.y - p3.y)*(target.x - p3.x) + (p3.x - p2.x)*(target.y - p3.y);
    int g2 = (p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y);
    return (float)g1/(float)g2;
}

float beta(Point& p1, Point& p2, Point& p3, Point& target){
    int b1 = (p3.y - p1.y)*(target.x - p3.x) + (p1.x - p3.x)*(target.y - p3.y);
    int b2 = (p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y);
    return (float)b1/(float)b2;
}
float alpha(Point& p1, Point& p2, Point& p3, Point& target){
    float b = beta(p1,p2,p3,target);
    float g = gamma(p1,p2,p3,target);
    return 1 - b - g;
}

rgb_t combine_color(float a, float b, float g, Point& p1, Point& p2, Point& p3){
    int r = (g * p1.r + b * p2.r + a * p3.r)/3*255;
    int gr = (g * p1.g + b * p2.g + a * p3.g)/3*255;
    int bl = (g * p1.b + b * p2.b + a * p3.b)/3*255;
    return make_colour(r, gr, bl);
}

int main(int argc, char** argv) {

    //50,50:1,0,0
    //600,20:0,1,0
    //300,400:0,0,1

    std::string p;

    std::cout << "Enter 3 points (enter a point as x,y:r,g,b):";
    std::cin >> p;
    Point p1;
    p1.stop(p);
    std::cin >> p;
    Point p2;
    p2.stop(p);
    std::cin >> p;
    Point p3;
    p3.stop(p);
    p1.print();
    p2.print();
    p3.print();
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    rgb_t color = make_colour(255, 255, 255);

    Point tmp_point(0,0,0,0,0);

    int max_x=max(3,p1.x,p2.x,p3.x);
    int max_y=max(3,p1.y,p2.y,p3.y);
    int min_x=min(3,p1.x,p2.x,p3.x);
    int min_y=min(3,p1.y,p2.y,p3.y);

    //std::cout << max_x << min_x << max_y << min_y <<std::endl;

    for(int i=min_y;i<max_y;i++){
        for(int j=min_x;j<max_x;j++){
            tmp_point.x = j;
            tmp_point.y = i;
            float g = gamma(p1,p2,p3,tmp_point);
            float b = beta(p1,p2,p3,tmp_point);
            float a = alpha(p1,p2,p3,tmp_point);
            if(g>0 && g<1 && b>0 && b<1 && a>0 && a<1){
                color = combine_color(a, b, g, p1, p2, p3);
                image.set_pixel(j,i,color);
            }

        }
    }
    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
