#ifndef MATRIX4_H
#define MATRIX4_H

#include <string>
#include <iostream>
#include <math.h>

#define PI 3.14159265

class Matrix4{
public:
    float values[16];

    Matrix4(){
        std::cout << "Matrix4 Constructor" <<std::endl;
        for(int i = 0; i < 16; ++i){
          values[i] = 0;
        }

    }

    ~Matrix4(){
        std::cout << "Matrix4 Destructor" <<std::endl;
    }

    void identity(){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }
    }

    void scale(float scale){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = (i == j);
        }
      }
      values[0] = scale;
      values[5] = scale;
      values[10] = scale;
    }

    void scale(float scale_x, float scale_y, float scale_z){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = (i == j);
        }
      }
      values[0] = scale_x;
      values[5] = scale_y;
      values[10] = scale_z;
    }

    void rotate_x(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }

      values[5] = cos(degree*PI/180);
      values[6] = -sin(degree*PI/180);
      values[9] = sin(degree*PI/180);
      values[10] = cos(degree*PI/180);
    }
    void rotate_y(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }

      values[0] = cos(degree*PI/180);
      values[8] = -sin(degree*PI/180);
      values[2] = sin(degree*PI/180);
      values[10] = cos(degree*PI/180);
    }
    void rotate_z(float degree){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }

      values[0] = cos(degree*PI/180);
      values[1] = -sin(degree*PI/180);
      values[4] = sin(degree*PI/180);
      values[5] = cos(degree*PI/180);
    }


    void rotate(float x, float y, float z){
      x = x*PI/180;
      y = y*PI/180;
      z = z*PI/180;
      values[0] = cos(z)*cos(y);
      values[1] = cos(z)*sin(y)*sin(x)-sin(z)*cos(x);
      values[2] = cos(z)*sin(y)*cos(x)+sin(z)*sin(x);
      values[3] = 0;
      values[4] = sin(z)*cos(y);
      values[5] = sin(z)*sin(y)*sin(x)+cos(z)*cos(x);
      values[6] = sin(z)*sin(y)*cos(x)-cos(z)*sin(x);
      values[7] = 0;
      values[8] = -sin(y);
      values[9] = cos(y)*sin(x);
      values[10] = cos(y)*cos(x);
      values[11] = 0;
      values[12] = 0;
      values[13] = 0;
      values[14] = 0;
      values[15] = 1;

      values[0] = cos(x)*cos(y);
      values[1] = (cos(x)*sin(y)*sin(z))-(sin(x)*cos(z));
      values[2] = cos(x)*sin(y)*cos(z)+(sin(x)*sin(z));
      values[3] = 0;
      values[4] = sin(x)*cos(y);
      values[5] = (sin(x)*sin(y)*sin(z))+(cos(x)*cos(z));
      values[6] = (sin(x)*sin(y)*cos(z))-(cos(x)*sin(z));
      values[7] = 0;
      values[8] = -sin(y);
      values[9] = cos(y)*sin(z);
      values[10] = cos(y)*cos(z);
      values[11] = 0;
      values[12] = 0;
      values[13] = 0;
      values[14] = 0;
      values[15] = 1;
    }

    void translate(float x, float y, float z){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }
      values[3] = x;
      values[7] = y;
      values[11] = z;
    }

    void perspective(){
      float n = -5;
      float f = 6;
      float l = -1;
      float r = 1;
      float t = 1;
      float b = -1;
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          values[i*4 + j] = i == j;
        }
      }
      values[10] = -f/(f-n);
      //values[10]=1;
      values[11] = -1;
      values[14] = -(f*n)/(f-n);
      values[15] = 0;

      /*
      values[0] = 2*abs(n)/(r-l);
      values[2] = (r+l)/(r-l);
      values[5] = 2*abs(n)/(t-b);
      values[6] = (t+b)/(t-b);
      values[10] = (abs(n)+abs(f))/(abs(n)-abs(f));
      values[11] = (2*abs(n)*abs(f))/(abs(n)-abs(f));
      values[14] = -1;
      values[15] = 0;
      */
    }

    void print(){
      for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
          std::cout << values[i*4 + j] << " \n"[j == 3];
        }
      }
    }

};

Matrix4 operator+(Matrix4& m1, Matrix4& m2){
	Matrix4 m4 = Matrix4();
  for(int i = 0; i < 16; ++i){
    m4.values[i] = m1.values[i] + m2.values[i];
  }

  return m4;
}

Matrix4 operator*(Matrix4& m1, Matrix4& m2){
	Matrix4 m4 = Matrix4();
  for(int i = 0; i < 16; ++i){
    float sum_i = 0;
    for(int j = 0; j < 4; ++j){
      sum_i = sum_i + (m1.values[i%4 + j*4] * m2.values[int(i/4)*4 + j]);
    }
    m4.values[i] = sum_i;
  }

  return m4;
}

#endif
