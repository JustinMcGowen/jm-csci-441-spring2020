#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform mat4 normTrans;
uniform vec3 lightPos;
uniform vec3 lightCol;
uniform float shininess;

out vec3 ourColor;

void main() {
    gl_Position = projection * camera * model * vec4(aPos, 1.0);


    vec4 lDir = vec4(lightPos,1) - vec4(aPos, 1);  //get direction of light
    lDir = normTrans * lDir;
    vec4 aNorm = normTrans * vec4(aNormal, 0);
    aNorm = aNorm/ sqrt(dot(aNorm, aNorm)); //normalize the normals
    float diff = dot(aNorm, lDir);
    if(diff < 0){
      diff =0;
    }if(diff > 1){
      diff = 1;
    }
    lDir = -1 * lDir;
    vec4 reflection = - lDir + 2*dot(lDir, aNorm)*aNorm;
    vec4 view = vec4(0,0,1,0);
    float spec = dot(view, reflection);
    if(spec < 0){
      spec =0;
    }
    if(spec > 1){
      spec = 1;
    }
    spec = pow(spec, shininess);
    //spec = pow(2,spec)/(pow(2,spec)+1);
    vec3 lcol = (spec) * lightCol * .00000001;
    ourColor =  (diff +.2) * aColor;
}
