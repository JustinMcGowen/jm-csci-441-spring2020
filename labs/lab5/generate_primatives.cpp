#ifndef _CSCI441__GEN_PRIM_H
#define _CSCI441__GEN_PRIM_H

#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>
#include <math.h>

class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(float h, float r, int s){
      //coords becomes an array of 6 * 3 * 4 * s floats representing the cylinder
      float ang = 6.2831853/s;

      //coords = new float[6 * 3 * 4 * s];

      int iter = 0;
      for(int i = 0; i < s; i++){
        coords.push_back(0); //coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0);
        coords.push_back(1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;

        coords.push_back(cos(ang*i)*r); // coords[iter++] = cos(ang*i)*r;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(sin(ang*i)*r); // coords[iter++] = sin(ang*i)*r;
        coords.push_back(0);
        coords.push_back(1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;

        coords.push_back(cos(ang*i+ang)*r); // coords[iter++] = cos(ang*i+ang)*r;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(sin(ang*i+ang)*r); // coords[iter++] = sin(ang*i+ang)*r;
        coords.push_back(0);
        coords.push_back(1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;

        coords.push_back(cos(ang*i)*r); // coords[iter++] = cos(ang*i)*r;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(sin(ang*i)*r); // coords[iter++] = sin(ang*i)*r;
        coords.push_back(cos(ang*i));
        coords.push_back(0);
        coords.push_back(sin(ang*i));
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(cos(ang*i+ang)*r); // coords[iter++] = cos(ang*i+ang)*r;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(sin(ang*i+ang)*r); // coords[iter++] = sin(ang*i+ang)*r;
        coords.push_back(cos(ang*i+ang));
        coords.push_back(0);
        coords.push_back(sin(ang*i+ang));
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(cos(ang*i+ang)*r); // coords[iter++] = cos(ang*i+ang)*r;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(sin(ang*i+ang)*r); // coords[iter++] = sin(ang*i+ang)*r;
        coords.push_back(cos(ang*i+ang));
        coords.push_back(0);
        coords.push_back(sin(ang*i+ang));
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(cos(ang*i)*r); // coords[iter++] = cos(ang*i)*r;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(sin(ang*i)*r); // coords[iter++] = sin(ang*i)*r;
        coords.push_back(cos(ang*i));
        coords.push_back(0);
        coords.push_back(sin(ang*i));
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(cos(ang*i+ang)*r); // coords[iter++] = cos(ang*i+ang)*r;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(sin(ang*i+ang)*r); // coords[iter++] = sin(ang*i+ang)*r;
        coords.push_back(cos(ang*i+ang));
        coords.push_back(0);
        coords.push_back(sin(ang*i+ang));
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(cos(ang*i)*r); // coords[iter++] = cos(ang*i)*r;
        coords.push_back(h/2); // coords[iter++] = h/2;
        coords.push_back(sin(ang*i)*r); // coords[iter++] = sin(ang*i)*r;
        coords.push_back(cos(ang*i));
        coords.push_back(0);
        coords.push_back(sin(ang*i));
        coords.push_back(1); // coords[iter++] = 1;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;

        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0);
        coords.push_back(-1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;

        coords.push_back(cos(ang*i)*r); // coords[iter++] = cos(ang*i)*r;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(sin(ang*i)*r); // coords[iter++] = sin(ang*i)*r;
        coords.push_back(0);
        coords.push_back(-1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;

        coords.push_back(cos(ang*i+ang)*r); // coords[iter++] = cos(ang*i+ang)*r;
        coords.push_back(-h/2); // coords[iter++] = -h/2;
        coords.push_back(sin(ang*i+ang)*r); // coords[iter++] = sin(ang*i+ang)*r;
        coords.push_back(0);
        coords.push_back(-1);
        coords.push_back(0);
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(0); // coords[iter++] = 0;
        coords.push_back(1); // coords[iter++] = 1;
      }

    }
};

class Box {
public:
    std::vector<float> coords;
    Box() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f
    } {}
};

class Beehive {
public:
    std::vector<float> coords;
    Beehive(float r, float d, int h){

      float ang = 6.2831853/d;
      float thet = 90/h;

      for(int i = 0; i<d; i++){
        float u = ang * i;

        for(int neg = -1;neg<2;neg+=2){

          for(int j= 0; j<h;j++){
            float v = thet*j;

            coords.push_back(r*cos(v)*cos(u));
            coords.push_back(r*cos(v)*sin(u));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v)*cos(u+ang));
            coords.push_back(r*cos(v)*sin(u+ang));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*sin(u+ang));
            coords.push_back(cos(v)*sin(u+ang));
            coords.push_back(sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u+ang));
            coords.push_back(r*cos(v+thet)*sin(u+ang));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v)*cos(u));
            coords.push_back(r*cos(v)*sin(u));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u));
            coords.push_back(r*cos(v+thet)*sin(u));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*sin(u));
            coords.push_back(cos(v+thet)*sin(u));
            coords.push_back(sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u+ang));
            coords.push_back(r*cos(v+thet)*sin(u+ang));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);
          }
        }
      }

    }
};

class Sphere {
public:
    std::vector<float> coords;
    Sphere(float r, float d, int h){

      float ang = 6.2831853/d;
      float thet = 6.2831853/4/h;

      for(int i = 0; i<d; i++){
        float u = ang * i;

        for(int neg = -1;neg<2;neg+=2){

          for(int j= 0; j<h;j++){
            float v = thet*j;

            coords.push_back(r*cos(v)*cos(u));
            coords.push_back(r*cos(v)*sin(u));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*cos(u));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(neg * sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v)*cos(u+ang));
            coords.push_back(r*cos(v)*sin(u+ang));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*cos(u+ang));
            coords.push_back(cos(v)*sin(u+ang));
            coords.push_back(neg * sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u+ang));
            coords.push_back(r*cos(v+thet)*sin(u+ang));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*cos(u+ang));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(neg * sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v)*cos(u));
            coords.push_back(r*cos(v)*sin(u));
            coords.push_back(neg * r*sin(v));
            coords.push_back(cos(v)*cos(u));
            coords.push_back(cos(v)*sin(u));
            coords.push_back(neg * sin(v));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u));
            coords.push_back(r*cos(v+thet)*sin(u));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*cos(u));
            coords.push_back(cos(v+thet)*sin(u));
            coords.push_back(neg * sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);

            coords.push_back(r*cos(v+thet)*cos(u+ang));
            coords.push_back(r*cos(v+thet)*sin(u+ang));
            coords.push_back(neg * r*sin(v+thet));
            coords.push_back(cos(v+thet)*cos(u+ang));
            coords.push_back(cos(v+thet)*sin(u+ang));
            coords.push_back(neg * sin(v+thet));
            coords.push_back(0);
            coords.push_back(0);
            coords.push_back(1);
          }
        }
      }

    }
};
#endif
/*
void box(float h, float w, float l, float* arr){
  //returns an array of 36 floats representing the cylinder

  float p[36*6];

  int iter = 0;

  for(int i = 0; i < 2; i++){
    float ni = (i-.5)*2;
    for(int j = 0; j< 2; j++){
      float nj = (j-.5)*2;
      for(int tri = 0; tri<3; tri++){
        for(int point = 0; point<3;point++){
          p[iter++] = ni*nj;
          p[iter++] = ni*(nj*-1);
          p[iter++] = ni;
        }
      }
      //std::cout << ni*nj << ni*(nj*-1) << ni <<std::endl;
    }
  }

  std::cout << *p;

}*/

//int main(void){
  /*
  float p[36];

  box(1,1,1,p);*/
  /*float p[6 * 3 * 4 * 10];
  cylinder(.5,.5,10,p);

  return 0;

}*/
