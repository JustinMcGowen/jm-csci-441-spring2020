#Proj01

This project is a simple maze game implementation. You play as a duck and the goal is to reach the green pad at the other side of the map, although the game does not keep score or completion of the map or anything. Although collision detection was not implemented, it still is fun to explore the maze in different perspectives.

###File reading
For this project, one of the main highlights is that all of the objects are being read from files. I used a lot of my work from lab6 as a starting point, I have been using the shape.h to do my file reading. The LoadFile object in this library reads in obj files and makes them usable by my program. Some of the nuances of obj are not picked up in my implementation of file reading, like texture coordinates, but the verticies and reference array are read in from the file. The obj file structure was a deciding factor when deciding what types of objects to render as well. I chose to use opengl to render vertex array objects.

I actually had some difficulty early on because I failed to realize that blender generates planes with more than 3 points (see, graphics is more than just triangles haha). I eventually overcame this obstacle however when generating the vertex array for triangles. Because points are stored in counter-clockwise order, I made an algorithm that takes a number of points (in CC order) and makes triangles to represent the whole surface. This works for my maze model, however, I don't think this algorithm works for concave planes.

These objects were also used when performing the character transformations to move the character through the maze and in calculating normals from a vertex's average plane normal.

###Views
This game has two view: one overhead and one first person. In both views, the character is actually being displayed, although I know in lots of first person video games, the developers might choose to not render the player or render a modified player model. Because there are no model animations, I feel like I could easily move this to a third person camera in the future, or maybe add the option to toggle between the two.

###Lighting
This was one of the areas, along with texturing that I wish I would have spent more time on. The lighting uses a very basic, single light system that works with object normals to display light differences. I would have also liked to implement object texturing instead of making models just single colors since this isn't super challenging as we saw from lab7 as long as our shapes are well parameterized for it.

###Controls
To control the player, you can use either the arrow keys or wasd to move along the ground. In order to look around, you can use the mouse (I started doing looking up and down, but the global rotation transformations were rendering some undesireable affects). In order to change perspectives, press space.
