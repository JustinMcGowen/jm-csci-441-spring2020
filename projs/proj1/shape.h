#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <stdio.h>
#include <string.h>

#include <cstdlib>
#include <vector>

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b,
        const Vector4& n=Vector4(1,0,0), bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);

    Vector4 normal = n.normalized();
    coords.push_back(normal.x());
    coords.push_back(normal.y());
    coords.push_back(normal.z());
}

class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,

        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0
    } {}

};

class DiscoCube1 {
public:
    std::vector<float> coords;
    std::vector<int> refs;
    DiscoCube1() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,

        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0
    }, refs{
        0,1,2,
        2,3,0,
        4,5,6,7,
        8,9,10,
        11,12,13,
        14,15,16,
        17,18,19,
        20,21,22,
        23,24,25,
        26,27,28,
        29,30,31,
        32,33

    }{}

};

class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(unsigned int n, float r, float g, float b) {
        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // add triangle vip1L, viH, vip1H
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);

            // add high triangle vi, vip1, 0
            Vector4 nh(0, 1, 0);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);

            // // add low triangle vi, vip1, 0
            Vector4 nl(0, -1, 0);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};


class Cone {
public:
    std::vector<float> coords;
    Cone(unsigned int n, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // // add low triangle vi, vip1, 0
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};

class Sphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    Sphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps/2.0; ++i) {
            for (int j = 0; j < n_steps; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }
};

class Torus {
    double x(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta) {
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }

};

class LoadFile {
public:
    std::vector<float> coords;
    std::vector<int> refs;
    std::vector<float> verticies;  //doesn't change from obj file
    std::vector<int> tris;        //doesn't change from obj file
    LoadFile() {

    }
    LoadFile(const char * path) {
      LoadFile f = LoadFile(path, 0,0,1);
      coords = f.coords;
      refs = f.refs;
    }
    LoadFile(const char * path, float r, float g, float b) {

        FILE * file = fopen(path, "r");
        if( file == NULL ){
            std::cout << "Cant open file !" << std::endl;
        }

        while( 1 ){

            char lineHeader[128];
            // read the first word of the line
            int res = fscanf(file, "%s", lineHeader);

            if (res == EOF){
                break; // EOF = End Of File
            }
            if ( strcmp( lineHeader, "v" ) == 0 ){
                //v -536.201721 -629.937256 -700.468323
                float v[3];
                fscanf(file, "%f %f %f\n", &v[0], &v[1], &v[2] );
                verticies.push_back(v[0]);
                verticies.push_back(v[1]);
                verticies.push_back(v[2]);
                verticies.push_back(r);
                verticies.push_back(g);
                verticies.push_back(b);
                verticies.push_back(v[0]);
                verticies.push_back(v[1]);
                verticies.push_back(v[2]);
            }else if ( strcmp( lineHeader, "vt" ) == 0 ){
              float v[3];
              fscanf(file, "%f %f %f\n", &v[0], &v[1], &v[2] );
            }else if ( strcmp( lineHeader, "vn" ) == 0 ){
              float v[3];
              fscanf(file, "%f %f %f\n", &v[0], &v[1], &v[2] );
            }else if ( strcmp( lineHeader, "f" ) == 0 ){
                //f 235 10 239
                int v[3];
                char str[1000];
                //char str[100];
                //scanf("%[^\n]", str);
                //fscanf(file, "%d %d %d\n", &v[0], &v[1], &v[2]);
                fscanf(file, "%[^\n]\n", &str);
                //str1 = str;
                if(strstr(str, "/")){
                  int face[20];
                  int iter = 0;

                  while(sscanf(str,"%d/%d/%d", &v[0], &v[1], &v[2]) == 3){
                    std::string s,line;
                    line = std::string(str);
                    s = std::to_string(v[0])+"/"+std::to_string(v[1])+"/"+std::to_string(v[2]);
                    size_t pos = line.find(s);
                    line.erase(pos, s.length());
                    strcpy(str, line.c_str());

                    face[iter++] = v[0]-1;

                    //tris.push_back(v[0]-1);
                    // tris.push_back(v[1]-1);
                    // tris.push_back(v[2]-1);
                  }
                  std::string s1 = "";
                  for(int i = 0; i < iter; i++){
                    s1 = s1 +std::to_string(face[i])+",";
                  }
                  int tmp_v[3];
                  tmp_v[1] = face[0];
                  tmp_v[0] = face[iter-1];
                  int last_dir = -1;

                  for(int i = 0; i < iter-2; i++){
                    tmp_v[2] = tmp_v[1];
                    tmp_v[1] = tmp_v[0];
                    if(last_dir < 0){
                      tmp_v[0] = face[(i/2)+1];
                      tris.push_back(tmp_v[0]);
                      tris.push_back(tmp_v[1]);
                      tris.push_back(tmp_v[2]);
                    }else{
                      tmp_v[0] = face[iter - (i/2)-2];
                      tris.push_back(tmp_v[1]);
                      tris.push_back(tmp_v[0]);
                      tris.push_back(tmp_v[2]);
                    }

                    last_dir *= -1;
                    // if(i <= iter-3){
                    //   tris.push_back(face[i]);
                    //   tris.push_back(face[i+1]);
                    //   tris.push_back(face[i+2]);
                    // }else if(i <= iter-2 ){
                    //   tris.push_back(face[i]);
                    //   tris.push_back(face[i+1]);
                    //   tris.push_back(face[0]);
                    // }else{
                    //   tris.push_back(face[i]);
                    //   tris.push_back(face[0]);
                    //   tris.push_back(face[1]);
                    // }
                  }
                }else{
                  sscanf(str, "%d %d %d\n", &v[0], &v[1], &v[2]);
                  tris.push_back(v[0]-1);
                  tris.push_back(v[1]-1);
                  tris.push_back(v[2]-1);
                }

            }
        }
        coords = verticies;
        refs = tris;
        // std::cout << refs.size() << std::endl;
        // std::cout << coords.size() << std::endl;

    }

    void flatShading(){
      coords.clear();
      //vector<T>().swap(x);
      refs.clear();


      float x1, x2, y1, y2, z1, z2, x3, y3, z3, nx, ny, nz;
      for(int i = 0;i < tris.size(); i+=3){
          x1 = verticies[tris[i]*9];
          y1 = verticies[tris[i]*9+1];
          z1 = verticies[tris[i]*9+2];
          x2 = verticies[tris[i+1]*9];
          y2 = verticies[tris[i+1]*9+1];
          z2 = verticies[tris[i+1]*9+2];
          x3 = verticies[tris[i+2]*9];
          y3 = verticies[tris[i+2]*9+1];
          z3 = verticies[tris[i+2]*9+2];

          x1 = x1-x3;
          y1 = y1-y3;
          z1 = z1-z3;
          x2 = x2-x3;
          y2 = y2-y3;
          z2 = z2-z3;

          nx = y1 * z2 - z1 * y2;
          ny = z1 * x2 - x1 * z2;
          nz = x1 * y2 - y1 * x2;
          for(int j = 0; j < 3; j++){
            coords.push_back(verticies[tris[i+j]*9]);
            coords.push_back(verticies[tris[i+j]*9+1]);
            coords.push_back(verticies[tris[i+j]*9+2]);
            coords.push_back(verticies[tris[i+j]*9+3]);
            coords.push_back(verticies[tris[i+j]*9+4]);
            coords.push_back(verticies[tris[i+j]*9+5]);
            coords.push_back(nx);
            coords.push_back(ny);
            coords.push_back(nz);
          }

          refs.push_back(i);
          refs.push_back(i+1);
          refs.push_back(i+2);

      }
    }

    void smoothShading(){
      int count[verticies.size()/9] = {0};
      for(int i = 0;i<verticies.size()/9;i++){
        verticies[i*9+6] = 0;
        verticies[i*9+7] = 0;
        verticies[i*9+8] = 0;

      }

      float x1, x2, y1, y2, z1, z2, x3, y3, z3, nx, ny, nz;

      for(int i = 0;i<tris.size();i+=3){
        x1 = verticies[tris[i]*9];
        y1 = verticies[tris[i]*9+1];
        z1 = verticies[tris[i]*9+2];
        x2 = verticies[tris[i+1]*9];
        y2 = verticies[tris[i+1]*9+1];
        z2 = verticies[tris[i+1]*9+2];
        x3 = verticies[tris[i+2]*9];
        y3 = verticies[tris[i+2]*9+1];
        z3 = verticies[tris[i+2]*9+2];

        x1 = x1-x3;
        y1 = y1-y3;
        z1 = z1-z3;
        x2 = x2-x3;
        y2 = y2-y3;
        z2 = z2-z3;

        nx = y1 * z2 - z1 * y2;
        ny = z1 * x2 - x1 * z2;
        nz = x1 * y2 - y1 * x2;

        for(int j = 0; j<3; j++){
          count[tris[i+j]] ++;
          verticies[tris[i+j]*9+6] += nx;
          verticies[tris[i+j]*9+7] += ny;
          verticies[tris[i+j]*9+8] += nz;
        }

      }
      for(int i = 0;i<verticies.size()/9;i++){
        verticies[i*9+6] /= count[i];
        verticies[i*9+7] /= count[i];
        verticies[i*9+8] /= count[i];

      }

      coords.swap(verticies);
      refs.swap(tris);
    }

    void unit_size(){
        float bx,by,bz,sx,sy,sz = 0;
        float x,y,z;
        for(int i = 0; i < coords.size()/9; i++){
            if(coords[i*9] > bx){
              bx = coords[i*9];
            }
            if(coords[i*9] < sx){
              sx = coords[i*9];
            }
            if(coords[i*9+1] > by){
              by = coords[i*9+1];
            }
            if(coords[i*9+1] < sy){
              sy = coords[i*9+1];
            }
            if(coords[i*9+2] > bz){
              bz = coords[i*9+2];
            }
            if(coords[i*9+2] < sz){
              sz = coords[i*9+2];
            }
        }
        x = bx - sx;
        y = by - sy;
        z = bz - sz;
        if(y > x){
          x = y;
        }
        if(z > x){
          x = z;
        }
        for(int i = 0; i < coords.size()/9; i++){
          coords[i*9] /= x;
          coords[i*9+1] /= x;
          coords[i*9+2] /= x;
        }
    }
    void scale_by(float scale){
        for(int i = 0;i<coords.size()/9;i++){
          coords[i*9] *= scale;
          coords[i*9+1] *= scale;
          coords[i*9+2] *= scale;

        }
    }
    void translate(float x, float y, float z){
        for(int i = 0;i<coords.size()/9;i++){
          coords[i*9] += x;
          coords[i*9+1] += y;
          coords[i*9+2] += z;

        }
    }
    void rotate(float x = 0, float y = 0, float z = 0){

    }
    void rotate(const Matrix4& rotation){
      float x,y,z;
      for(int i = 0;i<coords.size()/9;i++){
        x = coords[i*9];
        y = coords[i*9+1];
        z = coords[i*9+2];
        coords[i*9] = -rotation.values[0]*x + -rotation.values[1]*y + -rotation.values[2]*z;
        coords[i*9+1] = -rotation.values[4]*x + -rotation.values[5]*y + -rotation.values[6]*z;
        coords[i*9+2] = -rotation.values[8]*x + -rotation.values[9]*y + -rotation.values[10]*z;
        coords[i*9+6] = -rotation.values[0]*x + -rotation.values[1]*y + -rotation.values[2]*z;
        coords[i*9+7] = -rotation.values[4]*x + -rotation.values[5]*y + -rotation.values[6]*z;
        coords[i*9+8] = -rotation.values[8]*x + -rotation.values[9]*y + -rotation.values[10]*z;
      }
    }

    void transform(const Matrix4& rotation){
      float x,y,z;
      for(int i = 0;i<coords.size()/9;i++){
        x = coords[i*9];
        y = coords[i*9+1];
        z = coords[i*9+2];
        coords[i*9] = rotation.values[0]*x + rotation.values[1]*x + rotation.values[2]*x + rotation.values[3]*x;
        coords[i*9+1] = rotation.values[4]*y + rotation.values[5]*y + rotation.values[6]*y + rotation.values[7]*y;
        coords[i*9+2] = rotation.values[8]*z + rotation.values[9]*z + rotation.values[10]*z + rotation.values[11]*z;
      }
    }

    void combine_with(const LoadFile& obj){
        int my_size = coords.size()/9;
        for(int i = 0; i < obj.coords.size(); i++){
          coords.push_back(obj.coords[i]);
        }
        for(int i = 0; i < obj.refs.size(); i++){
          refs.push_back(obj.refs[i] + my_size);
        }
    }
};

#endif
