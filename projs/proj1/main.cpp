#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <shader.h>
#include <matrix4.h>
#include <matrix3.h>
#include <vector4.h>
#include <uniform.h>

#include "shape.h"
//#include "model.h"
// #include "camera.h"
// #include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix4 processModel(GLFWwindow *window) {
    Matrix4 trans;

    const float TRANS = .01;

    // TRANSLATE
    if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, 0, TRANS); }
    else if (isPressed(window, GLFW_KEY_W)) { trans.translate(0, 0, TRANS); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, 0, -TRANS); }
    else if (isPressed(window, GLFW_KEY_S)) { trans.translate(0, 0, -TRANS); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_A)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_D)) { trans.translate(-TRANS, 0, 0); }

    return trans;
}

Matrix4 processView(const Matrix4& view, GLFWwindow *window){
  Matrix4 yaw, pitch;
  const float ROT = .03;
  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);
  glfwSetCursorPos(window, SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
  //std::cout << SCREEN_WIDTH/2 - xpos << "," << SCREEN_HEIGHT/2 - ypos << std::endl;

  yaw.rotate_y(ROT * (xpos - SCREEN_WIDTH/2));
  pitch.rotate_x(ROT * (SCREEN_HEIGHT/2 - ypos));
  //std::cout << yaw;

  return  view *  yaw ;
}

int special_keys(GLFWwindow *window){
  if (isPressed(window, GLFW_KEY_SPACE)) { return 1; }
  else if (isPressed(window, GLFW_KEY_Q)) { return 2; }

  return 0;

}

Matrix4 transform_to_view(const Matrix4& trans1, const Matrix4& view1){
  Matrix4 trans = trans1;
  Matrix4 view = view1;
  view = view.transpose();
  //view.values[3] = view1.values[8];
  //view.values[8] = view1.values[3];
  trans.values[0] = 0;
  trans.values[5] = 0;
  trans.values[10] = 0;
  trans.values[15] = 0;
  trans = view * trans;
  trans.values[0] = 1;
  trans.values[5] = 1;
  trans.values[10] = 1;
  trans.values[15] = 1;


  return trans;
}

void processInput(Matrix4& model, Matrix4& view, int& spec_key, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) ) {
    //if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    view = processView(view, window);
    Matrix4 tmp_transform;
    tmp_transform = processModel(window);
    tmp_transform = transform_to_view(tmp_transform, view);
    //std::cout << view << std::endl;
    model = tmp_transform * model;
    spec_key = special_keys(window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void){
  GLFWwindow* window;

  glfwSetErrorCallback(errorCallback);

  /* Initialize the library */
  if (!glfwInit()) { return -1; }

  glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
  if (!window) {
      glfwTerminate();
      return -1;
  }

  /* Make the window's context current */
  glfwMakeContextCurrent(window);

  // tell glfw what to do on resize
  glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

  if (!gladLoadGL()) {
      std::cout << "Failed to initialize OpenGL context" << std::endl;
      glfwTerminate();
      return 0;
  }

  //loading the maze from file
  LoadFile maze = LoadFile("../models/maze.obj",0.0,0.5,0.0);
  maze.flatShading();
  maze.smoothShading();
  maze.unit_size();
  maze.scale_by(1.5);

  LoadFile floor = LoadFile("../models/floor.obj",0.2,0.2,0.2);
  floor.flatShading();
  floor.smoothShading();
  floor.translate(0,-0.03,0);

  LoadFile goal = LoadFile("../models/floor.obj",0.0,1.0,0.1);
  goal.flatShading();
  goal.smoothShading();
  goal.scale_by(.06);
  goal.translate(-0.629,-0.029,-0.75);

  LoadFile player = LoadFile("../models/duck.obj", 0.8,0.8,.1);
  player.flatShading();
  player.smoothShading();
  player.unit_size();
  player.scale_by(.05);
  Matrix4 initial_rotate = Matrix4();
  initial_rotate.rotate_y(-90);
  player.rotate(initial_rotate);
  Vector4 model_offset(0,0,0.02,0);
  player.translate(model_offset.values[0],0,model_offset.values[2]);

  LoadFile player_mobile;
  player_mobile.combine_with(player);

  LoadFile all = LoadFile();
  all.combine_with(player_mobile);
  all.combine_with(maze);
  all.combine_with(floor);
  all.combine_with(goal);


//------------------------------
  int size = all.coords.size()*sizeof(float);
  int idxSize = all.refs.size()*sizeof(int);

  GLuint vbo;
  GLuint vao;
  GLuint ebo;

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, size, all.coords.data(), GL_DYNAMIC_DRAW);

  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, idxSize, all.refs.data(), GL_DYNAMIC_DRAW);

  // describe vertex layout
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
          (void*)(0*sizeof(float)));
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
          (void*)(3*sizeof(float)));
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
          (void*)(6*sizeof(float)));
  glEnableVertexAttribArray(2);

  //------------------------

  Shader shader("../vert.glsl", "../frag.glsl");

  // and use z-buffering
  glEnable(GL_DEPTH_TEST);
  //disable cursor
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  int input_keys;

  Matrix4 model;
  model.translate(-.65,0,-.7);

  Matrix4 view_over;
  view_over.look_at(Vector4(0,1,0),Vector4(0,0,0),Vector4(0,0,1));
  Matrix4 view_fp;
  int view_choice = 0;
  int view_changed = 0;

  Matrix4 projection;
  projection.perspective(45, 1, .01, 10);

  while (!glfwWindowShouldClose(window)) {
      // process input
      processInput(model, view_fp, input_keys, window);

      if(input_keys == 1){ //spacebar pressed
        if(view_changed != 1){ //if it was not changed last frame
          view_choice = (view_choice + 1)%2;
        }
        view_changed = 1;
      }else{
        view_changed = 0;
      }

      if(view_choice == 0){
        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "camera", view_fp);
      }else{
        Uniform::set(shader.id(), "model", Matrix4());
        Uniform::set(shader.id(), "camera", view_over);
      }

      shader.use();
      Uniform::set(shader.id(), "projection", projection);
      Uniform::set(shader.id(), "itModel", Matrix3());
      Uniform::set(shader.id(), "eye", Vector4());
      Uniform::set(shader.id(), "lightPos", Vector4(0,1,0,0));

      glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


      player_mobile = LoadFile();
      player_mobile.combine_with(player);
      player_mobile.translate(-model_offset.values[0],0,-model_offset.values[2]);
      player_mobile.rotate(view_fp);
      player_mobile.translate((view_fp.transpose() * model_offset).values[0],0,(view_fp.transpose() * model_offset).values[2]);
      player_mobile.translate(-model.values[12], 0, -model.values[14]);
      // for(int i = 0; i< 16; i++){
      //   std::cout << model.values[i] << ",";
      // }
      // std::cout << std::endl ;

      LoadFile all = LoadFile();
      all.combine_with(player_mobile);
      all.combine_with(maze);
      all.combine_with(goal);
      all.combine_with(floor);

      //resets new triangles if they have changed
      glBufferData(GL_ARRAY_BUFFER, size, all.coords.data(), GL_DYNAMIC_DRAW);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
      glDrawElements(GL_TRIANGLES, idxSize, GL_UNSIGNED_INT, 0);
      glBindVertexArray(vao);



      glfwSwapBuffers(window);
      glfwPollEvents();

  }
  glfwTerminate();
  return 0;
}
